"""Pelican generator configuration.

Python 3 only.
"""

import logging
from pathlib import Path
from urllib.parse import urljoin

REPO_ROOT = Path(__file__).parent

# Site metadata
SITENAME = "khardix.cz"
SITEURL = "https://khardix.cz"
AUTHOR = "Jan “Khardix” Staněk"
TIMEZONE = "Europe/Prague"

# Defaults
DEFAULT_LANG = "en-basiceng"
DEFAULT_METADATA = {"lang": DEFAULT_LANG}
DEFAULT_DATE = "fs"  # Use file system date
DEFAULT_DATE_FORMAT = "%Y-%m-%d"

# Site theme
THEME = REPO_ROOT / "theme"
DIRECT_TEMPLATES = ["index"]

JINJA_ENVIRONMENT = {
    "extensions": ["jinja2.ext.i18n"],
    "lstrip_blocks": True,
    "trim_blocks": True,
}

# URLs ↔ paths mappings
PAGE_PATHS = [""]  # pages live in content root
ARTICLE_PATHS = ["article"]  # articles live in subdir

PAGE_URL = PAGE_SAVE_AS = "{slug}.html"
ARTICLE_URL_BASE = ARTICLE_SAVE_AS_BASE = "{date:%Y-%m-%d}-{slug}.html"
DRAFT_URL = DRAFT_SAVE_AS = ".draft/{slug}.html"
DRAFT_PAGE_URL = DRAFT_PAGE_SAVE_AS = DRAFT_URL

INDEX_SAVE_AS = ""  # no untranslated index

AUTHOR_URL = AUTHOR_SAVE_AS = ""  # disabled

# Local I/O paths
PATH = REPO_ROOT / "content"
OUTPUT_PATH = REPO_ROOT / "output"

# Output generation settings
DELETE_OUTPUT_DIRECTORY = True
OUTPUT_SOURCES_EXTENSION = ".text"

# Translations
DATE_FORMATS = {"en": ("en_US", "%A %B %-d %Y %Z"), "cs": ("cs_CZ", "%A %-d. %B %Y %Z")}
ARTICLE_TRANSLATION_ID = PAGE_TRANSLATION_ID = "translation_id"

I18N_COMMON_OVERRIDES = {}
I18N_SUBSITES = {
    "en": dict(
        I18N_COMMON_OVERRIDES,
        INDEX_SAVE_AS="article/index.html",
        ARTICLE_SAVE_AS=f"article/{ARTICLE_SAVE_AS_BASE}",
        ARTICLE_URL=f"article/{ARTICLE_URL_BASE}",
    ),
    "cs": dict(
        I18N_COMMON_OVERRIDES,
        INDEX_SAVE_AS="clanek/index.html",
        ARTICLE_SAVE_AS=f"clanek/{ARTICLE_SAVE_AS_BASE}",
        ARTICLE_URL=f"clanek/{ARTICLE_URL_BASE}",
    ),
}
I18N_UNTRANSLATED_ARTICLES = I18N_UNTRANSLATED_PAGES = "remove"

# RSS Feeds
FEED_DOMAIN = SITEURL
FEED_RSS = FEED_RSS_URL = "rss.xml"

FEED_MAX_ITEMS = 1024
RSS_FEED_SUMMARY_ONLY = False

FEED_ALL_ATOM = CATEGORY_FEED_ATOM = AUTHOR_FEED_ATOM = AUTHOR_FEED_RSS = None
TRANSLATION_FEED_ATOM = None

# Other settings
TYPOGRIFY = True

# Plugins and plugin configuration
PLUGIN_ROOT = REPO_ROOT / "plugins"
PLUGIN_PATHS = [PLUGIN_ROOT / "local", PLUGIN_ROOT / "community"]
PLUGINS = ["auto_translation_id", "i18n_subsites"]

# Extra templating filters


def urljoin_relative(base: str, url: str) -> str:
    """Joins URL base and path. Respects RELATIVE_URL settings."""

    relative_base = base in {"", "."}
    if relative_base:
        return url
    else:
        return urljoin(base, url)


unicode_flag_map = {"cs": "🇨🇿", "en": "🇺🇸"}

JINJA_FILTERS = {
    "urljoin": urljoin_relative,
    "unicode_flag": unicode_flag_map.__getitem__,
}


# Workarounds
def do_not_report_untranslated(record: logging.LogRecord):
    """In current setup, multiple "originals" are expected."""
    return not record.msg.startswith("There are %s original (not translated)")


util_logger = logging.getLogger("pelican.utils")
util_logger.addFilter(do_not_report_untranslated)
