.PHONY: develop server

TRANS_BASEDIR := theme/translations/
TRANS_SOURCES := $(shell find $(TRANS_BASEDIR) -name 'messages.po')
TRANSLATIONS  := $(TRANS_SOURCES:.po=.mo)

OUTPUT_DEPS := content/ theme/ $(TRANSLATIONS)

develop: $(OUTPUT_DEPS)
	pelican --delete-output-directory --relative-urls $<

server: $(OUTPUT_DEPS)
	pelican --relative-urls --listen --autoreload $<

%.mo: %.po
	@pybabel compile --output-file=$@ --input-file=$<
