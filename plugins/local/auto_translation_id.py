"""Automatic assignment of translation IDs.

This plugin automatically assigns default translation ID
to those objects which already do not have one.
It can be configured by setting `AUTO_TRANSLATION_ID_SOURCE`,
which defaults to `slug` and names the existing attribute
to get the default ID from.
"""

import logging
from functools import singledispatch
from typing import Any, Optional

from pelican import signals
from pelican.contents import Article, Page

logger = logging.getLogger(__name__)


# Dispatch "table" for determining the target attribute
@singledispatch
def id_name(content: Any) -> Optional[str]:
    """Determine the name of the translation identifier.

    Returns:
        str: Name of the metadata attribute that binds translations together.
        None: The queried content type cannot be translated.
    """

    logger.warn("Cannot determine translation ID for `%s`", type(content).__name__)


id_name.register(Article)(lambda content: content.settings["ARTICLE_TRANSLATION_ID"])
id_name.register(Page)(lambda content: content.settings["PAGE_TRANSLATION_ID"])


def set_default_translation_id(content: Any) -> None:
    """Sets the translation identifier if not already present."""

    source = content.settings.get("AUTO_TRANSLATION_ID_SOURCE", "slug")
    target = id_name(content)

    try:
        if not hasattr(content, target):
            setattr(content, target, getattr(content, source))
    except AttributeError as no_source_err:
        raise ValueError(
            f"Cannot set default translation id: source attribute `{source}` missing."
        ) from no_source_err


def register() -> None:
    """Register signal handlers"""

    signals.content_object_init.connect(set_default_translation_id)
